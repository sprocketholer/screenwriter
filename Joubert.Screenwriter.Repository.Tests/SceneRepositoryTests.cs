﻿// <copyright file="SceneRepositoryTests.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Repository.Tests
{
    using System;
    using System.Data.SqlClient;
    using Common.Interfaces.Repository;
    using Common.Models;
    using NUnit.Framework;
    using SqlTestPrep;

    /// <summary>
    /// Scene Repository unit tests
    /// </summary>
    [TestFixture]
    public class SceneRepositoryTests
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneRepositoryTests"/> class
        /// </summary>
        public SceneRepositoryTests()
        {
            this._sqlLoader = new SqlLoader();
        }

        #endregion

        #region Pirvate Properties

        /// <summary>
        /// Gets an instance of the <see cref="SqlLoader"/> class
        /// </summary>
        private SqlLoader _sqlLoader { get; }

        /// <summary>
        /// Gets or sets an implementation of the <see cref="ISceneRepository"/> class
        /// </summary>
        private ISceneRepository _sceneRepository { get; set; }

        #endregion

        #region Setup/Teardown

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            try
            {
                this._sqlLoader.Setup("Screenwriter");
            }
            catch
            {
                this._sqlLoader.TearDown("Screenwriter");
            }

            this._sceneRepository = new SceneRepository();
        }

        /// <summary>
        /// Teardown method
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._sqlLoader.TearDown("Screenwriter");
            this._sceneRepository = null;
        }

        #endregion

        #region Tests

        /// <summary>
        /// Tests Get - Success
        /// </summary>
        [Test]
        public void SceneRepository_Get_Success()
        {
            var result = this._sceneRepository.Get(Guid.Parse("34a35e66-e2a6-468d-8361-dbe0ab58ed36"));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// Tests Get - Fail
        /// </summary>
        [Test]
        public void SceneRepository_Get_Fail()
        {
            var result = this._sceneRepository.Get(Guid.Parse("2915b085-3ce3-4307-a38f-316ef2b3b7f2"));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(0));
        }

        /// <summary>
        /// Tests GetSingle - Success
        /// </summary>
        [Test]
        public void SceneRepository_GetSingle_Success()
        {
            var result = this._sceneRepository.GetSingle(Guid.Parse("34a35e66-e2a6-468d-8361-dbe0ab58ed36"), Guid.Parse("54106701-374f-43c4-8b79-546db4e9ca96"));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.SceneId, Is.EqualTo(Guid.Parse("54106701-374f-43c4-8b79-546db4e9ca96")));
        }

        /// <summary>
        /// Tests GetSingle - Fail
        /// </summary>
        [Test]
        public void SceneRepository_GetSingle_Fail()
        {
            var result = this._sceneRepository.GetSingle(Guid.Parse("34a35e66-e2a6-468d-8361-dbe0ab58ed36"), Guid.Parse("2915b085-3ce3-4307-a38f-316ef2b3b7f2"));

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests Insert - Success
        /// </summary>
        [Test]
        public void SceneRepository_Insert_Success()
        {
            var scene = new Scene()
            {
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                SceneNumber = 5,
                SceneName = "The Rebels Surrender",
                Throughline = "Convince Jose to Surrender to the Union",
                SceneDescription = "Fletcher tries to convice Jose to surrender to the Union",
                SceneBeats = "Beat 1, Beat 2, Beat 3"
            };

            var result = this._sceneRepository.Insert(scene);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.SceneId, Is.EqualTo(scene.SceneId));
        }

        /// <summary>
        /// Tests Insert - Fail
        /// </summary>
        [Test]
        public void SceneRepository_Insert_Fail()
        {
            var scene = new Scene()
            {
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                SceneNumber = 5,
                SceneName = null,
                Throughline = "Convince Jose to Surrender to the Union",
                SceneDescription = "Fletcher tries to convice Jose to surrender to the Union",
                SceneBeats = "Beat 1, Beat 2, Beat 3"
            };

            Assert.Throws<SqlException>(() => this._sceneRepository.Insert(scene));
        }

        /// <summary>
        /// Tests Update - Success
        /// </summary>
        [Test]
        public void SceneRepository_Update_Success()
        {
            var scene = new Scene()
            {
                SceneId = Guid.Parse("329f3c88-41b0-4f7b-8913-53d808bdacd1"),
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                SceneNumber = 5,
                SceneName = "The Rebels Surrender",
                Throughline = "Convince Jose to Surrender to the Union",
                SceneDescription = "Fletcher tries to convice Jose to surrender to the Union",
                SceneBeats = "Beat 1, Beat 2, Beat 3"
            };

            var result = this._sceneRepository.Update(scene);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.SceneId, Is.EqualTo(scene.SceneId));
        }

        /// <summary>
        /// Tests Update - Fail
        /// </summary>
        [Test]
        public void SceneRepository_Update_Fail()
        {
            var scene = new Scene()
            {
                SceneId = Guid.Parse("329f3c88-41b0-4f7b-8913-53d808bdacd1"),
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                SceneNumber = 5,
                SceneName = null,
                Throughline = "Convince Jose to Surrender to the Union",
                SceneDescription = "Fletcher tries to convice Jose to surrender to the Union",
                SceneBeats = "Beat 1, Beat 2, Beat 3"
            };

            Assert.Throws<SqlException>(() => this._sceneRepository.Update(scene));
        }

        /// <summary>
        /// Tests Delete - Success
        /// </summary>
        [Test]
        public void SceneRepository_Delete_Success()
        {
            var result = this._sceneRepository.Delete(Guid.Parse("34a35e66-e2a6-468d-8361-dbe0ab58ed36"), Guid.Parse("329f3c88-41b0-4f7b-8913-53d808bdacd1"));

            Assert.That(result, Is.True);
        }

        #endregion
    }
}
