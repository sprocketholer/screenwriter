﻿// <copyright file="ScreenplayRepositoryTests.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Repository.Tests
{
    using System;
    using System.Data.SqlClient;
    using Common.Interfaces.Repository;
    using Common.Models;
    using NUnit.Framework;
    using SqlTestPrep;

    /// <summary>
    /// Screenplay Repository unit tests
    /// </summary>
    [TestFixture]
    public class ScreenplayRepositoryTests
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenplayRepositoryTests"/> class
        /// </summary>
        public ScreenplayRepositoryTests()
        {
            this._sqlLoader = new SqlLoader();
        }

        #endregion

        #region Pirvate Properties

        /// <summary>
        /// Gets an instance of the <see cref="SqlLoader"/> class
        /// </summary>
        private SqlLoader _sqlLoader { get; }

        /// <summary>
        /// Gets or sets an implementation of the <see cref="IScreenplayRepository"/> class
        /// </summary>
        private IScreenplayRepository _screenplayRepository { get; set; }

        #endregion

        #region Setup/Teardown

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            try
            {
                this._sqlLoader.Setup("Screenwriter");
            }
            catch
            {
                this._sqlLoader.TearDown("Screenwriter");
            }

            this._screenplayRepository = new ScreenplayRepository();
        }

        /// <summary>
        /// Teardown method
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._sqlLoader.TearDown("Screenwriter");
            this._screenplayRepository = null;
        }

        #endregion

        #region Tests

        /// <summary>
        /// Tests Get - Success
        /// </summary>
        [Test]
        public void ScreenplayRepository_Get_Success()
        {
            var result = this._screenplayRepository.Get();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// Tests GetSingle - Success
        /// </summary>
        [Test]
        public void ScreenplayRepository_GetSingle_Success()
        {
            var result = this._screenplayRepository.GetSingle(Guid.Parse("34a35e66-e2a6-468d-8361-dbe0ab58ed36"));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Title, Is.EqualTo("Raiders of the Lost Ark"));
        }

        /// <summary>
        /// Tests GetSingle - Fail
        /// </summary>
        [Test]
        public void ScreenplayRepository_GetSingle_Fail()
        {
            var result = this._screenplayRepository.GetSingle(Guid.Parse("0157fcf7-3f69-4e10-a54e-f6b77729bd18"));

            Assert.That(result, Is.Null);
        }

        /// <summary>
        /// Tests Insert - Success
        /// </summary>
        [Test]
        public void ScreenplayRepository_Insert_Success()
        {
            var screenplay = new Screenplay()
            {
                Title = "House of Games",
                AuthorName = "David Mamet",
                ScreenplayVersion = "37",
                Copyright = "1987",
                LogLine = "A psychiatrist comes to the aid of a compulsive gambler",
                Synopsis = "A psychiatrist comes to the aid of a compulsive gambler and is led by a smooth-talking grifter into the shadowy but compelling world of stings, scams, and con men."
            };

            var result = this._screenplayRepository.Insert(screenplay);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ScreenplayId, Is.EqualTo(screenplay.ScreenplayId));
        }

        /// <summary>
        /// Tests Insert - Success
        /// </summary>
        [Test]
        public void ScreenplayRepository_Insert_Fail()
        {
            var screenplay = new Screenplay()
            {
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                Title = "House of Games",
                AuthorName = "David Mamet",
                ScreenplayVersion = "37",
                Copyright = "1987",
                LogLine = "A psychiatrist comes to the aid of a compulsive gambler",
                Synopsis = "A psychiatrist comes to the aid of a compulsive gambler and is led by a smooth-talking grifter into the shadowy but compelling world of stings, scams, and con men."
            };

            Assert.Throws<SqlException>(() => this._screenplayRepository.Insert(screenplay));
        }

        /// <summary>
        /// Tests Update - Success
        /// </summary>
        [Test]
        public void ScreenplayRepository_Update_Success()
        {
            var screenplay = new Screenplay()
            {
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                Title = "House of Games",
                AuthorName = "David Mamet",
                ScreenplayVersion = "37",
                Copyright = "1987",
                LogLine = "A psychiatrist comes to the aid of a compulsive gambler",
                Synopsis = "A psychiatrist comes to the aid of a compulsive gambler and is led by a smooth-talking grifter into the shadowy but compelling world of stings, scams, and con men."
            };

            var result = this._screenplayRepository.Update(screenplay);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ScreenplayId, Is.EqualTo(screenplay.ScreenplayId));
        }

        /// <summary>
        /// Tests Update - Fail
        /// </summary>
        [Test]
        public void ScreenplayRepository_Update_Fail()
        {
            var screenplay = new Screenplay()
            {
                ScreenplayId = Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"),
                Title = null,
                AuthorName = null,
                ScreenplayVersion = "37",
                Copyright = "1987",
                LogLine = "A psychiatrist comes to the aid of a compulsive gambler",
                Synopsis = "A psychiatrist comes to the aid of a compulsive gambler and is led by a smooth-talking grifter into the shadowy but compelling world of stings, scams, and con men."
            };

            Assert.Throws<SqlException>(() => this._screenplayRepository.Update(screenplay));
        }

        /// <summary>
        /// Tests Delete - Success
        /// </summary>
        [Test]
        public void ScreenplayRepository_Delete_Success()
        {
            var result = this._screenplayRepository.Delete(Guid.Parse("acd0051e-6c0f-49b1-b73b-bd0834dc4b43"));

            Assert.That(result, Is.True);
        }

        #endregion
    }
}
