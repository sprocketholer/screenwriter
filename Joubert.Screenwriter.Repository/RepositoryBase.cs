﻿// <copyright file="RepositoryBase.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Repository
{
    using Joubert.Screenwriter.Common.Logging;

    /// <summary>
    /// Repository base class (Abstract)
    /// </summary>
    public abstract class RepositoryBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase"/> class
        /// </summary>
        protected RepositoryBase()
        {
            this._logger = new LogWriter();
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets or sets an instance of the <see cref="LogWriter"/> class
        /// </summary>
        protected LogWriter _logger { get; set; }

        #endregion
    }
}
