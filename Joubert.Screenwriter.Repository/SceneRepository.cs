// <copyright file="SceneRepository.cs" company="Stephen Joubert">
//    Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Common.Interfaces.Repository;
    using Common.Models;

    /// <summary>
    /// Scene Repository class
    /// </summary>
    public class SceneRepository : RepositoryBase, ISceneRepository
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneRepository"/> class
        /// </summary>
        public SceneRepository() : base()
        {
        }

        #endregion

        #region Public Methods

        /// <inheritdoc />
        public IList<Scene> Get(Guid screenplayId)
        {
            this._logger.Info("SceneRepository.Get(screenplayId: {0}) called", screenplayId);
            
            var sql = "SELECT " +
                "SceneId, ScreenplayId, SceneNumber, SceneName, Throughline, SceneDescription, SceneBeats " + 
                "FROM Scene " +
                "WHERE ScreenplayId = @ScreenplayId";
            
            List<Scene> scenes = new List<Scene>();
            
            this._logger.Debug("SceneRepository.Get(screenplayId: {1}), SQL: {0}", sql, screenplayId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue("@ScreenplayId", screenplayId);

                    connection.Open();
                    
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            scenes.Add(this.GetScene(dataReader));
                        }
                    }
                }
            }
            
            this._logger.Debug("SceneRepository.Get(ScreenplayId: {0}) Returned {1} records", screenplayId, scenes.Count);
            
            return scenes;
        }

        /// <inheritdoc />
        public Scene GetSingle(Guid screenplayId, Guid sceneId)
        {
            this._logger.Info("SceneRepository.GetSingle(sceneId: {0}) called", sceneId);
            
            var sql = "SELECT " +
                "SceneId, ScreenplayId, SceneNumber, SceneName, Throughline, SceneDescription, SceneBeats " + 
                "FROM Scene " + 
                "WHERE SceneId = @SceneId " +
                "AND ScreenplayId = @ScreenplayId";
            
            Scene scene = null;
            
            this._logger.Debug("SceneRepository.GetSingle(sceneId: {1}), SQL: {0}", sql, sceneId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    command.Parameters.AddWithValue("@SceneId", sceneId);
                    command.Parameters.AddWithValue("@ScreenplayId", screenplayId);

                    connection.Open();
                    
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            scene = this.GetScene(dataReader);
                        }
                    }
                }
            }
            
            if (scene == null)
            {
                this._logger.Debug("SceneRepository.GetSingle(SceneId: {0}) returned null", sceneId);
            }
            else
            {
                this._logger.Debug("SceneRepository.GetSingle(SceneId: {0}) returned 1 record", sceneId);
            }
            
            return scene;
        }

        /// <inheritdoc />
        public Scene Insert(Scene scene)
        {
            this._logger.Info("SceneRepository.Insert(sceneId: {0}) called", scene.SceneId);
            
            var sql = "INSERT INTO Scene(SceneId, ScreenplayId, SceneNumber, SceneName, Throughline, SceneDescription, SceneBeats) " +
                "VALUES (@SceneId, @ScreenplayId, @SceneNumber, @SceneName, @Throughline, @SceneDescription, @SceneBeats)";

            this._logger.Debug("SceneRepository.Insert(sceneId: {1}), SQL: {0}", sql, scene.SceneId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    this.GetParameters(command, scene);
                    
                    connection.Open();
                    
                    command.ExecuteNonQuery();
                    
                    this._logger.Debug("SceneRepository.Insert(SceneId: {0}) successful", scene.SceneId);
                }
            }
            
            return scene;
        }

        /// <inheritdoc />
        public Scene Update(Scene scene)
        {
            this._logger.Info("SceneRepository.Update(sceneId: {0}) called", scene.SceneId);
            
            var sql = "UPDATE Scene SET " + 
                "ScreenplayId = @ScreenplayId " +
                ",SceneNumber = @SceneNumber " +
                ",SceneName = @SceneName " +
                ",Throughline = @Throughline " +
                ",SceneDescription = @SceneDescription " +
                ",SceneBeats = @SceneBeats " +
                "WHERE SceneId = @SceneId";
            
            this._logger.Debug("SceneRepository.Update(sceneId: {1}), SQL: {0}", sql, scene.SceneId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    this.GetParameters(command, scene);
                    
                    connection.Open();
                    
                    command.ExecuteNonQuery();

                    this._logger.Debug("SceneRepository.Update(SceneId: {0}) successful", scene.SceneId);
                }
            }
            
            return scene;
        }

        /// <inheritdoc />
        public bool Delete(Guid screenplayId, Guid sceneId)
        {
            this._logger.Info("SceneRepository.Delete(sceneId: {0}) called.", sceneId);
            
            var sql = "DELETE FROM Scene " + 
                "WHERE SceneId = @SceneId " +
                "AND ScreenplayId = @ScreenplayId";
            
            this._logger.Debug("SceneRepository.Delete(sceneId: {1}), SQL: {0}", sql, sceneId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    command.Parameters.AddWithValue("@SceneId", sceneId);
                    command.Parameters.AddWithValue("@ScreenplayId", screenplayId);

                    connection.Open();
                    
                    command.ExecuteNonQuery();
                }
            }

            this._logger.Debug("SceneRepository.Delete(SceneId: {0}) successful.", sceneId);
            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Converts a data reader record into a Scene
        /// </summary>
        /// <param name="dataReader">An instances of <see cref="SqlDataReader"/></param>
        /// <returns>A list of <see cref="Scene"/> instances</returns>
        private Scene GetScene(SqlDataReader dataReader)
        {
            return new Scene()
            {
                SceneId = (Guid)dataReader["SceneId"],
                ScreenplayId = (Guid)dataReader["ScreenplayId"],
                SceneNumber = (int)dataReader["SceneNumber"],
                SceneName = (string)dataReader["SceneName"],
                Throughline = (string)(dataReader.IsDBNull(4) ? null : dataReader["Throughline"]),
                SceneDescription = (string)(dataReader.IsDBNull(5) ? null : dataReader["SceneDescription"]),
                SceneBeats = (string)(dataReader.IsDBNull(6) ? null : dataReader["SceneBeats"]),
            };
        }

        /// <summary>
        /// Converts a Scene into SQL Parameters 
        /// </summary>
        /// <param name="command">An instance of <see cref="SqlCommand"/></param>
        /// <param name="scene">An instance of <see cref="Scene"/></param>
        /// <returns>An instance of <see cref="SqlCommand"/> with parameters added</returns>
        private SqlCommand GetParameters(SqlCommand command, Scene scene)
        {
            command.Parameters.AddWithValue("@SceneId", scene.SceneId);
            command.Parameters.AddWithValue("@ScreenplayId", scene.ScreenplayId);
            command.Parameters.AddWithValue("@SceneNumber", scene.SceneNumber);
            command.Parameters.AddWithValue("@SceneName", scene.SceneName);
            command.Parameters.AddWithValue("@Throughline", string.IsNullOrEmpty(scene.Throughline) ? DBNull.Value : (object)scene.Throughline);
            command.Parameters.AddWithValue("@SceneDescription", string.IsNullOrEmpty(scene.SceneDescription) ? DBNull.Value : (object)scene.SceneDescription);
            command.Parameters.AddWithValue("@SceneBeats", string.IsNullOrEmpty(scene.SceneBeats) ? DBNull.Value : (object)scene.SceneBeats);

            return command;
        }

        #endregion
    }
}
