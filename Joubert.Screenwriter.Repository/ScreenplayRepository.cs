// <copyright file="ScreenplayRepository.cs" company="Stephen Joubert">
//    Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Common.Interfaces.Repository;
    using Common.Models;

    /// <summary>
    /// Screenplay Repository class
    /// </summary>
    public class ScreenplayRepository : RepositoryBase, IScreenplayRepository
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenplayRepository"/> class
        /// </summary>
        public ScreenplayRepository() : base()
        {
        }

        #endregion

        #region Public Methods

        /// <inheritdoc />
        public IList<Screenplay> Get()
        {
            this._logger.Info("ScreenplayRepository.Get called");
            
            var sql = "SELECT " +
                "ScreenplayId, Title, AuthorName, ScreenplayVersion, Copyright, LogLine, Synopsis " + 
                "FROM Screenplay";
            
            List<Screenplay> screenplays = new List<Screenplay>();
            
            this._logger.Debug("ScreenplayRepository.Get, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    connection.Open();
                    
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            screenplays.Add(this.GetScreenplay(dataReader));
                        }
                    }
                }
            }
            
            this._logger.Debug("ScreenplayRepository.Get Returned {0} records", screenplays.Count);
            
            return screenplays;
        }

        /// <inheritdoc />
        public Screenplay GetSingle(Guid screenplayId)
        {
            this._logger.Info("ScreenplayRepository.GetSingle(screenplayId: {0}) called.", screenplayId);
            
            var sql = "SELECT " +
                "ScreenplayId, Title, AuthorName, ScreenplayVersion, Copyright, LogLine, Synopsis " + 
                "FROM Screenplay " + 
                "WHERE ScreenplayId = @ScreenplayId";
            
            Screenplay screenplay = null;
            
            this._logger.Debug("ScreenplayRepository.GetSingle(screenplayId: {1}), SQL: {0}", sql, screenplayId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    command.Parameters.AddWithValue("@ScreenplayId", screenplayId);
                    
                    connection.Open();
                    
                    using (var dataReader = command.ExecuteReader())
                    {
                        if (dataReader.Read())
                        {
                            screenplay = this.GetScreenplay(dataReader);
                        }
                    }
                }
            }

            if (screenplay == null)
            {
                this._logger.Debug("ScreenplayRepository.GetSingle(ScreenplayID: {0}) returned null", screenplayId);
            }
            else
            {
                this._logger.Debug("ScreenplayRepository.GetSingle(ScreenplayID: {0}) returned 1 record", screenplayId);
            }

            return screenplay;
        }

        /// <inheritdoc />
        public Screenplay Insert(Screenplay screenplay)
        {
            this._logger.Info("ScreenplayRepository.Insert(screenplayId: {0}) called", screenplay.ScreenplayId);
            
            var sql = "INSERT INTO Screenplay(ScreenplayId, Title, AuthorName, ScreenplayVersion, Copyright, LogLine, Synopsis) " +
                "VALUES (@ScreenplayId, @Title, @AuthorName, @ScreenplayVersion, @Copyright, @LogLine, @Synopsis)";

            this._logger.Debug("ScreenplayRepository.Insert(screenplayId: {1}), SQL: {0}", sql, screenplay.ScreenplayId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    this.GetParameters(command, screenplay);
                    
                    connection.Open();
                    
                    command.ExecuteNonQuery();
                    
                    this._logger.Debug("ScreenplayRepository.Insert(ScreenplayId: {0}) successful", screenplay.ScreenplayId);
                }
            }
            
            return screenplay;
        }

        /// <inheritdoc />
        public Screenplay Update(Screenplay screenplay)
        {
            this._logger.Info("ScreenplayRepository.Update(screenplayId: {0}) called", screenplay.ScreenplayId);
            
            var sql = "UPDATE Screenplay SET " + 
                "Title = @Title " +
                ",AuthorName = @AuthorName " +
                ",ScreenplayVersion = @ScreenplayVersion " +
                ",Copyright = @Copyright " +
                ",LogLine = @LogLine " +
                ",Synopsis = @Synopsis " +
                "WHERE ScreenplayId = @ScreenplayId";
            
            this._logger.Debug("ScreenplayRepository.Update(screenplayId: {1}), SQL: {0}", sql, screenplay.ScreenplayId);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    this.GetParameters(command, screenplay);
                    
                    connection.Open();
                    
                    command.ExecuteNonQuery();

                    this._logger.Debug("ScreenplayRepository.Insert(ScreenplayId: {0}) successful", screenplay.ScreenplayId);
                }
            }
            
            return screenplay;
        }

        /// <inheritdoc />
        public bool Delete(Guid screenplayId)
        {
            this._logger.Info("ScreenplayRepository.Delete(screenplayId: {0}) called", screenplayId);
            
            var sql = "DELETE FROM Screenplay " + 
                "WHERE ScreenplayId = @ScreenplayId";
            
            this._logger.Debug("ScreenplayRepository.Delete, SQL: {0}", sql);
            
            using (SqlConnection connection = new SqlConnection(Settings.MainConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;
                    
                    command.Parameters.AddWithValue("@ScreenplayId", screenplayId);
                    
                    connection.Open();
                    
                    command.ExecuteNonQuery();
                }
            }

            this._logger.Debug("ScreenplayRepository.Delete(ScreenplayId: {0}) successful", screenplayId);

            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Converts a data reader record into a Screenplay
        /// </summary>
        /// <param name="dataReader">An instance of the <see cref="SqlDataReader"/> class</param>
        /// <returns>A list of <see cref="Screenplay"/> instances</returns>
        private Screenplay GetScreenplay(SqlDataReader dataReader)
        {
            return new Screenplay()
            {
                ScreenplayId = (Guid)dataReader["ScreenplayId"],
                Title = (string)dataReader["Title"],
                AuthorName = (string)dataReader["AuthorName"],
                ScreenplayVersion = (string)(dataReader.IsDBNull(3) ? null : dataReader["ScreenplayVersion"]),
                Copyright = (string)(dataReader.IsDBNull(4) ? null : dataReader["Copyright"]),
                LogLine = (string)(dataReader.IsDBNull(5) ? null : dataReader["LogLine"]),
                Synopsis = (string)(dataReader.IsDBNull(6) ? null : dataReader["Synopsis"]),
            };
        }

        /// <summary>
        /// Converts a Screenplay into SQL Parameters 
        /// </summary>
        /// <param name="command">An instance of the <see cref="SqlCommand"/> class</param>
        /// <param name="screenplay">An instance of the <see cref="Screenplay"/> class</param>
        /// <returns>An instance of <see cref="SqlCommand"/> with parameters added</returns>
        private SqlCommand GetParameters(SqlCommand command, Screenplay screenplay)
        {
            command.Parameters.AddWithValue("@ScreenplayId", screenplay.ScreenplayId);
            command.Parameters.AddWithValue("@Title", screenplay.Title);
            command.Parameters.AddWithValue("@AuthorName", screenplay.AuthorName);
            command.Parameters.AddWithValue("@ScreenplayVersion", string.IsNullOrEmpty(screenplay.ScreenplayVersion) ? DBNull.Value : (object)screenplay.ScreenplayVersion);
            command.Parameters.AddWithValue("@Copyright", string.IsNullOrEmpty(screenplay.Copyright) ? DBNull.Value : (object)screenplay.Copyright);
            command.Parameters.AddWithValue("@LogLine", string.IsNullOrEmpty(screenplay.LogLine) ? DBNull.Value : (object)screenplay.LogLine);
            command.Parameters.AddWithValue("@Synopsis", string.IsNullOrEmpty(screenplay.Synopsis) ? DBNull.Value : (object)screenplay.Synopsis);

            return command;
        }

        #endregion
    }
}
