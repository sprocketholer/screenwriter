﻿// <copyright file="SceneService.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Service
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;

    /// <summary>
    /// Scene Service class
    /// </summary>
    public class SceneService : ServiceBase, ISceneService
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneService"/> class
        /// </summary>
        /// <param name="sceneRepository">An implementation of the <see cref="ISceneRepository"/> interface</param>
        public SceneService(ISceneRepository sceneRepository)
        {
            this._sceneRepository = sceneRepository;
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an implementation of the <see cref="ISceneRepository"/> interface
        /// </summary>
        private ISceneRepository _sceneRepository { get; }

        #endregion

        #region Public Methods

        /// <inheritdoc />
        public IList<Scene> Get(Guid screenplayId)
        {
            this._logger.Info("SceneService.Get(screenplayId:{0}) called.", screenplayId);
            return this._sceneRepository.Get(screenplayId);
        }

        /// <inheritdoc />
        public Scene GetSingle(Guid screenplayId, Guid sceneId)
        {
            this._logger.Info("SceneService.GetSingle(sceneId:{0}) called.", sceneId);
            return this._sceneRepository.GetSingle(screenplayId, sceneId);
        }

        /// <inheritdoc />
        public Scene Save(Scene scene)
        {
            this._logger.Info("SceneService.Save(sceneId:{0}) called.", scene.SceneId);
            var sceneTest = this._sceneRepository.GetSingle(scene.ScreenplayId, scene.SceneId);

            if (sceneTest == null)
            {
                this._logger.Info("SceneService.Save(sceneId:{0}) called SceneRepository.Insert", scene.SceneId);
                scene = this._sceneRepository.Insert(scene);
            }
            else
            {
                this._logger.Info("SceneService.Save(sceneId:{0}) called SceneRepository.Update", scene.SceneId);
                scene = this._sceneRepository.Update(scene);
            }

            return scene;
        }

        /// <inheritdoc />
        public bool Delete(Guid screenplayId, Guid sceneId)
        {
            this._logger.Info("SceneService.Delete(sceneId:{0}) called.", sceneId);
            return this._sceneRepository.Delete(screenplayId, sceneId);
        }

        #endregion
    }
}
