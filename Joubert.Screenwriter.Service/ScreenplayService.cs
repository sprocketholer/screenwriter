﻿// <copyright file="ScreenplayService.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;

    /// <summary>
    /// Screenplay Service class
    /// </summary>
    public class ScreenplayService : ServiceBase, IScreenplayService
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenplayService"/> class
        /// </summary>
        /// <param name="screenplayRepository">An implementation of the <see cref="IScreenplayRepository"/> interface</param>
        /// <param name="sceneService">An implementation of the <see cref="ISceneService"/> interface</param>
        public ScreenplayService(IScreenplayRepository screenplayRepository, ISceneService sceneService)
        {
            this._screenplayRepository = screenplayRepository;
            this._sceneService = sceneService;
        }

        #endregion

        #region Private Properties 

        /// <summary>
        /// Gets an implementation of the <see cref="IScreenplayRepository"/> interface
        /// </summary>
        private IScreenplayRepository _screenplayRepository { get; }

        /// <summary>
        /// Gets an implementation of the <see cref="ISceneService"/> interface
        /// </summary>
        private ISceneService _sceneService { get; }

        #endregion

        #region Public Methods
       
        /// <inheritdoc />
        public IList<Screenplay> Get()
        {
            this._logger.Info("ScreenplayService.Get called.");
            return this._screenplayRepository.Get();
        }

        /// <inheritdoc />
        public Screenplay GetSingle(Guid screenplayId, bool includeScenes = false)
        {
            this._logger.Info("ScreenplayService.GetSingle(screenplayId:{0}) called.", screenplayId);
            var screenplay = this._screenplayRepository.GetSingle(screenplayId);

            if (includeScenes)
            {
                this._logger.Info("ScreenplayService.GetSingle(screenplayId:{0}) include scenes called", screenplayId);
                screenplay.Scenes = this._sceneService.Get(screenplay.ScreenplayId).ToList();
            }

            return screenplay;
        }

        /// <inheritdoc />
        public Screenplay Save(Screenplay screenplay)
        {
            this._logger.Info("ScreenplayService.Save(screenplayId:{0}) called.", screenplay.ScreenplayId);

            var screenplayTest = this.GetSingle(screenplay.ScreenplayId);

            if (screenplayTest == null)
            {
                this._logger.Info("ScreenplayService.Save(screenplayId:{0}) called ScreenplayRepository.Insert.", screenplay.ScreenplayId);
                screenplay = this._screenplayRepository.Insert(screenplay);
            }
            else
            {
                this._logger.Info("ScreenplayService.Save(screenplayId:{0}) called ScreenplayRepository.Update.", screenplay.ScreenplayId);
                screenplay = this._screenplayRepository.Update(screenplay);
            }

            return screenplay;
        }

        /// <inheritdoc />
        public bool Delete(Guid screenplayId)
        {
            this._logger.Info("ScreenplayService.Delete(screenplayId:{0}) called.", screenplayId);
            return this._screenplayRepository.Delete(screenplayId);
        }

        #endregion
    }
}
