﻿// <copyright file="ServiceBase.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Service
{
    using Joubert.Screenwriter.Common.Logging;

    /// <summary>
    /// Service Base class (Abstract)
    /// </summary>
    public abstract class ServiceBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceBase"/> class
        /// </summary>
        protected ServiceBase()
        {
            this._logger = new LogWriter();
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets or sets an instance of the <see cref="LogWriter"/> class
        /// </summary>
        protected LogWriter _logger { get; set; }

        #endregion
    }
}
