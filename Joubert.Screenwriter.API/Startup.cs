﻿// <copyright file="Startup.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API
{
    using System;
    using System.IO;
    using System.Reflection;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Repository;
    using Service;
    using Swashbuckle.AspNetCore.Swagger;

    /// <summary>
    /// Startup class
    /// </summary>
    public class Startup
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class
        /// </summary>
        /// <param name="configuration">An implementation of the <see cref="IConfiguration"/> interface</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets an implementation of the<see cref= "IConfiguration" /> interface
        /// </summary>
        public IConfiguration Configuration { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">An implementation of the <see cref="IServiceCollection"/> interface</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Setup Repositories
            services.AddTransient<IScreenplayRepository, ScreenplayRepository>();
            services.AddTransient<ISceneRepository, SceneRepository>();

            // Setup Services
            services.AddTransient<IScreenplayService, ScreenplayService>();
            services.AddTransient<ISceneService, SceneService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(
                    "v1", 
                    new Info
                    {
                        Title = "Screenwriter",
                        Version = "v1",
                        Description = "Screenwriter Sample Code API",
                        TermsOfService = "None",
                        Contact = new Contact
                        {
                            Name = "Stephen Joubert",
                            Email = "stephenjoubert@yahoo.com",
                        }
                    });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">An implementation of the <see cref="IApplicationBuilder"/> interface</param>
        /// <param name="env">An implementation of the <see cref="IHostingEnvironment"/> interface</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Screenwriter API v1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }

        #endregion
    }
}
