﻿// <copyright file="ScreenplayController.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces.Service;
    using Common.Models;
    using Joubert.Screenwriter.Common.Logging;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Screenplay controller class
    /// </summary>
    [Route("api/screenplays")]
    [ApiController]
    public class ScreenplayController : LoggingControllerBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreenplayController"/> class
        /// </summary>
        /// <param name="screenplayService">An implementation of the <see cref="IScreenplayService"/> interface</param>
        public ScreenplayController(IScreenplayService screenplayService) : base()
        {
            this._screenplayService = screenplayService;
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an implementation of the <see cref="IScreenplayService"/> interface
        /// </summary>
        private IScreenplayService _screenplayService { get; }

        #endregion

        #region Endpoints

        /// <summary>
        /// Gets a list of all screenplays
        /// GET: API/screenplays
        /// </summary>
        /// <returns>An instance of the <see cref="ActionResult"/> of a list of the <see cref="Screenplay"/> class</returns>
        [HttpGet]
        public ActionResult<List<Screenplay>> Get()
        {
            this._logger.Info("GET: api/screenplays called.");

            try
            {
                return this._screenplayService.Get().ToList();
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Gets a specific screenplay
        /// </summary>
        /// GET: API/screenplays/{screenplayId}?includescenes=true
        /// <param name="screenplayId">The screenplay Id</param>
        /// <param name="includeScenes">Indicates whether or not to include scenes</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of an instance of the <see cref="Screenplay"/> class</returns>
        [HttpGet("{screenplayId}", Name = "Get")]
        public ActionResult<Screenplay> Get(Guid screenplayId, [FromQuery] bool includeScenes)
        {
            this._logger.Info(string.Format("GET: api/screenplays/{0}?includescenes={1} called.", screenplayId, includeScenes));

            try
            {
                return this._screenplayService.GetSingle(screenplayId, includeScenes);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Posts a new screenplay to be saved
        /// POST: API/screenplay
        /// </summary>
        /// <param name="screenplay">An instance of the <see cref="Screenplay"/> class</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of an instance of the <see cref="Screenplay"/> class</returns>
        [HttpPost]
        public ActionResult<Screenplay> Post([FromBody] Screenplay screenplay)
        {
            this._logger.Info("POST: api/screenplays called.");

            try
            {
                return this._screenplayService.Save(screenplay);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Puts an updated screenplay to be saved
        /// PUT: API/screenplays/{screenplayId}
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="screenplay">An instance of the <see cref="Screenplay"/> class</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of an instance of the <see cref="Screenplay"/> class</returns>
        [HttpPut("{screenplayId}")]
        public ActionResult<Screenplay> Put(Guid screenplayId, [FromBody] Screenplay screenplay)
        {
            this._logger.Info(string.Format("PUT: api/screenplays/{0} called.", screenplayId));

            if (screenplayId != screenplay.ScreenplayId)
            {
                return this.StatusCode(StatusCodes.Status403Forbidden);
            }

            try
            {
                return this._screenplayService.Save(screenplay);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Deletes a screenplay
        /// DELETE: API/screenplays/{screenplayId}
        /// </summary>
        /// <param name="screenplayId">A Screenplay Id</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of type bool</returns>
        [HttpDelete("{screenplayId}")]
        public ActionResult<bool> Delete(Guid screenplayId)
        {
            this._logger.Info("Delete: api/screenplays/{0} called.", screenplayId);

            try
            {
                return this._screenplayService.Delete(screenplayId);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion
    }
}
