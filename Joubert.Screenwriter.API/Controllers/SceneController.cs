﻿// <copyright file="SceneController.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Joubert.Screenwriter.Common.Interfaces.Service;
    using Joubert.Screenwriter.Common.Logging;
    using Joubert.Screenwriter.Common.Models;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Scene controller class
    /// </summary>
    [Route("api/screenplays/{screenplayId}/scenes")]
    [ApiController]
    public class SceneController : LoggingControllerBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneController"/> class
        /// </summary>
        /// <param name="sceneService">An implementation of the <see cref="ISceneService"/> interface</param>
        public SceneController(ISceneService sceneService) : base()
        {
            this._sceneService = sceneService;
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an implementation of the <see cref="ISceneService"/> interface
        /// </summary>
        private ISceneService _sceneService { get; }

        #endregion

        #region Endpoints

        /// <summary>
        /// Gets a the scenes for a screenplay
        /// GET: API/screenplays/{screenplayId}/scenes
        /// </summary>
        /// <param name="screenplayId">A Screenplay Id</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of a list of the <see cref="Scene"/> class</returns>
        [HttpGet]
        public ActionResult<List<Scene>> Get(Guid screenplayId) 
        {
            this._logger.Info("GET : api/screenplays/{0}/scenes called.");

            try
            {
                return this._sceneService.Get(screenplayId).ToList();
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Gets a scene
        /// GET: API/screenplays/{screenplayId}/scenes/{sceneId}
        /// </summary>
        /// <param name="screenplayId">A Screenplay Id</param>
        /// <param name="sceneId">A Scene Id</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of the <see cref="Scene"/> class</returns>
        [HttpGet("{sceneId}")]
        public ActionResult<Scene> GetSingle(Guid screenplayId, Guid sceneId)
        {
            this._logger.Info("GET : api/screenplays/{0}/scenes/{1} called.", screenplayId, sceneId);

            try
            {
                return this._sceneService.GetSingle(screenplayId, sceneId);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Posts a new scene to be saved
        /// POST: API/screenplays/{screenplayId}/scenes
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="scene">An instance of the <see cref="Scene"/> class</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of the <see cref="Scene"/> class</returns>
        [HttpPost]
        public ActionResult<Scene> Post(Guid screenplayId, [FromBody] Scene scene)
        {
            this._logger.Info("POST : api/screenplays/{0}/scenes called.");

            if (screenplayId != scene.ScreenplayId)
            {
                return this.StatusCode(StatusCodes.Status403Forbidden);
            }

            try
            {
                return this._sceneService.Save(scene);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Puts a updated scene to be saved
        /// PUT: API/screenplays/{screenplayId}/scenes/{sceneId}
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="sceneId">A scene Id</param>
        /// <param name="scene">An instance of the <see cref="Scene"/> class</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of the <see cref="Scene"/> class</returns>
        [HttpPut("{sceneId}")]
        public ActionResult<Scene> Put(Guid screenplayId, Guid sceneId, [FromBody] Scene scene)
        {
            this._logger.Info("PUT : api/screenplays/{0}/scenes/{1} called.", screenplayId, sceneId);

            if (screenplayId != scene.ScreenplayId)
            {
                return this.StatusCode(StatusCodes.Status403Forbidden);
            }

            if (sceneId != scene.SceneId)
            {
                return this.StatusCode(StatusCodes.Status403Forbidden);
            }

            try
            {
                return this._sceneService.Save(scene);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Deletes a scene
        /// DELETE: API/screenplays/{screenplayId}/scenes/{sceneId}
        /// </summary>
        /// <param name="screenplayId">A Screenplay Id</param>
        /// <param name="sceneId">A scene Id</param>
        /// <returns>An instance of the <see cref="ActionResult"/> of type bool</returns>
        [HttpDelete("{sceneId}")]
        public ActionResult<bool> Delete(Guid screenplayId, Guid sceneId)
        {
            this._logger.Info("GET : api/screenplays/{0}/scenes/{1} called.", screenplayId, sceneId);

            try
            {
                return this._sceneService.Delete(screenplayId, sceneId);
            }
            catch (Exception e)
            {
                this._logger.Error(LogBuilder.GetLog(e));
                return this.StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        #endregion
    }
}