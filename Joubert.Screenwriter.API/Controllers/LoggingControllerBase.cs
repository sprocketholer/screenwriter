﻿// <copyright file="LoggingControllerBase.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API.Controllers
{
    using Common.Logging;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Logging controller base class (Abstract)
    /// </summary>
    public abstract class LoggingControllerBase : ControllerBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggingControllerBase"/> class
        /// </summary>
        protected LoggingControllerBase()
        {
            this._logger = new LogWriter();
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets or sets an instance of the <see cref="LogWriter"/> class
        /// </summary>
        protected LogWriter _logger { get; set; }

        #endregion
    }
}
