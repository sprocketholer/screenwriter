﻿// <copyright file="DefaultController.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Default controller class
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        /// <summary>
        /// Get - Startup message
        /// </summary>
        /// <returns>An instance of the <see cref="ActionResult"/> class</returns>
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Screenwriter API is running!";
        }
    }
}