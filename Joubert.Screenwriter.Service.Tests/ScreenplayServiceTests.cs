﻿// <copyright file="ScreenplayServiceTests.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Service.Tests
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Screenplay Service unit tests
    /// </summary>
    [TestFixture]
    public class ScreenplayServiceTests
    {
        #region Private Properties

        /// <summary>
        /// Gets or sets an implementation of the <see cref="IScreenplayService"/> interface
        /// </summary>
        private IScreenplayService _screenplayService { get; set; }

        #endregion

        #region Setup/Teardown

        /// <summary>
        /// Setup Method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            // Setup Scene Service
            var sceneService = Substitute.For<ISceneService>();

            var scene1 = new Scene() { SceneId = Guid.Parse("a1d8135d-b662-469a-b0c2-e86b1752da2f"), ScreenplayId = Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2") };
            var scene2 = new Scene() { SceneId = Guid.Parse("606be0e1-e7e3-4772-b68b-7e4575cbb542"), ScreenplayId = Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2") };

            var scenes = new List<Scene>() { scene1, scene2 };
            var scenesEmpty = new List<Scene>();

            sceneService.Get(Arg.Any<Guid>()).Returns(scenesEmpty);
            sceneService.Get(Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2")).Returns(scenes);

            // Setup Screenplay Repository
            var screenplayRespository = Substitute.For<IScreenplayRepository>();

            var screenplay1 = new Screenplay() { ScreenplayId = Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2") };
            var screenplay2 = new Screenplay() { ScreenplayId = Guid.Parse("fd0347b1-fa43-4e4e-ad99-f0c443f6e3fb") };

            Screenplay screenplayNull = null;

            screenplayRespository.GetSingle(Arg.Any<Guid>()).Returns(screenplayNull);
            screenplayRespository.GetSingle(Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2")).Returns(screenplay1);
            screenplayRespository.GetSingle(Guid.Parse("fd0347b1-fa43-4e4e-ad99-f0c443f6e3fb")).Returns(screenplay2);
            screenplayRespository.Insert(Arg.Any<Screenplay>()).Returns(screenplay1);
            screenplayRespository.Update(Arg.Any<Screenplay>()).Returns(screenplay2);

            this._screenplayService = new ScreenplayService(screenplayRespository, sceneService);
        }

        /// <summary>
        /// Teardown method
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._screenplayService = null;
        }

        #endregion

        #region Tests

        /// <summary>
        /// Tests Get Single with Scenes - Success
        /// </summary>
        [Test]
        public void Screenplay_GetSingle_With_Scenes_Success()
        {
            var result = this._screenplayService.GetSingle(Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2"), true);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ScreenplayId, Is.EqualTo(Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2")));
            Assert.That(result.Scenes.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// Tests Get Single without Scenes - Success
        /// </summary>
        [Test]
        public void Screenplay_GetSingle_Without_Scenes_Success()
        {
            var result = this._screenplayService.GetSingle(Guid.Parse("fd0347b1-fa43-4e4e-ad99-f0c443f6e3fb"));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ScreenplayId, Is.EqualTo(Guid.Parse("fd0347b1-fa43-4e4e-ad99-f0c443f6e3fb")));
            Assert.That(result.Scenes.Count, Is.EqualTo(0));
        }

        /// <summary>
        /// Tests Save Insert - Success
        /// </summary>
        [Test]
        public void Screenplay_Save_Insert_Success()
        {
            var screenplay = new Screenplay() { ScreenplayId = Guid.Parse("d0096dc3-ce62-4132-b048-83cb172edb34") };
            var result = this._screenplayService.Save(screenplay);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ScreenplayId, Is.EqualTo(Guid.Parse("f242735f-b460-472b-b7e3-b968413a37a2")));
        }

        /// <summary>
        /// Tests Save Update - Success
        /// </summary>
        [Test]
        public void Screenplay_Save_Update_Success()
        {
            var screenplay = new Screenplay() { ScreenplayId = Guid.Parse("fd0347b1-fa43-4e4e-ad99-f0c443f6e3fb") };
            var result = this._screenplayService.Save(screenplay);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.ScreenplayId, Is.EqualTo(Guid.Parse("fd0347b1-fa43-4e4e-ad99-f0c443f6e3fb")));
        }

        #endregion
    }
}
