﻿// <copyright file="SceneServiceTests.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Service.Tests
{
    using System;
    using Common.Interfaces.Repository;
    using Common.Interfaces.Service;
    using Common.Models;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Scene Service unit tests
    /// </summary>
    [TestFixture]
    public class SceneServiceTests
    {
        #region Private Properties

        /// <summary>
        /// Gets or sets an implementation of the <see cref="ISceneService"/> interface
        /// </summary>
        private ISceneService _sceneService { get; set; }

        #endregion

        #region Setup/Teardown
        
        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var sceneRepository = Substitute.For<ISceneRepository>();

            var sceneInsert = new Scene() { SceneId = Guid.Parse("a1d8135d-b662-469a-b0c2-e86b1752da2f"), ScreenplayId = Guid.Parse("9ad58ecd-9a7a-4cb7-85d8-c0f67d1ff9ef") };
            var sceneUpdate = new Scene() { SceneId = Guid.Parse("606be0e1-e7e3-4772-b68b-7e4575cbb542"), ScreenplayId = Guid.Parse("9ad58ecd-9a7a-4cb7-85d8-c0f67d1ff9ef") };
            Scene sceneNull = null;

            sceneRepository.Insert(Arg.Any<Scene>()).Returns(sceneInsert);
            sceneRepository.Update(Arg.Any<Scene>()).Returns(sceneUpdate);
            sceneRepository.GetSingle(Arg.Any<Guid>(), Arg.Any<Guid>()).Returns(sceneNull);
            sceneRepository.GetSingle(Guid.Parse("9ad58ecd-9a7a-4cb7-85d8-c0f67d1ff9ef"), Guid.Parse("606be0e1-e7e3-4772-b68b-7e4575cbb542")).Returns(sceneUpdate);

            this._sceneService = new SceneService(sceneRepository);
        }

        /// <summary>
        /// Teardown method
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._sceneService = null;
        }

        #endregion

        #region Tests

        /// <summary>
        /// Tests Save Insert - Success
        /// </summary>
        [Test]
        public void SceneService_Save_Insert_Success()
        {
            var sceneInsert = new Scene() { SceneId = Guid.Parse("a1d8135d-b662-469a-b0c2-e86b1752da2f"), ScreenplayId = Guid.Parse("9ad58ecd-9a7a-4cb7-85d8-c0f67d1ff9ef") };

            var result = this._sceneService.Save(sceneInsert);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.SceneId, Is.EqualTo(Guid.Parse("a1d8135d-b662-469a-b0c2-e86b1752da2f")));
        }

        /// <summary>
        /// Tests Save Update - Success
        /// </summary>
        [Test]
        public void SceneService_Save_Update_Success()
        {
            var sceneUpdate = new Scene() { SceneId = Guid.Parse("606be0e1-e7e3-4772-b68b-7e4575cbb542"), ScreenplayId = Guid.Parse("9ad58ecd-9a7a-4cb7-85d8-c0f67d1ff9ef") };

            var result = this._sceneService.Save(sceneUpdate);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.SceneId, Is.EqualTo(Guid.Parse("606be0e1-e7e3-4772-b68b-7e4575cbb542")));
        }

        #endregion
    }
}
