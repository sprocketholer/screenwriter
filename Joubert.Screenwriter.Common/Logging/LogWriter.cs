﻿// <copyright file="LogWriter.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Logging
{
    using NLog;

    /// <summary>
    /// Log writer class
    /// </summary>
    public class LogWriter
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogWriter"/> class
        /// </summary>
        public LogWriter()
        {
            this._logger = LogManager.GetCurrentClassLogger();
            this._loggingServiceName = Settings.LoggingServiceName;
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets an instance of the <see cref="Logger"/> class
        /// </summary>
        private Logger _logger { get; }

        /// <summary>
        /// Gets the name of the service doing the logging
        /// </summary>
        private string _loggingServiceName { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds debug level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Debug(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Debug(message);
        }

        /// <summary>
        /// Adds debug level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Debug(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Debug(message, args);
        }

        /// <summary>
        /// Adds info level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Info(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Info(message);
        }

        /// <summary>
        /// Adds info level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Info(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Info(message, args);
        }

        /// <summary>
        /// Adds warn level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Warn(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Warn(message);
        }

        /// <summary>
        /// Adds warn level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Warn(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Warn(message, args);
        }

        /// <summary>
        /// Adds Error level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Error(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Error(message);
        }

        /// <summary>
        /// Adds Error level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Error(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Error(message, args);
        }

        /// <summary>
        /// Adds Fatal level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Fatal(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Fatal(message);
        }

        /// <summary>
        /// Adds Fatal level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Fatal(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Fatal(message, args);
        }

        #endregion
    }
}
