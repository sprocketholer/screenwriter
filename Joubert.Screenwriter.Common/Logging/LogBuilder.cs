﻿// <copyright file="LogBuilder.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Logging
{
    using System;
    using System.Text;

    /// <summary>
    /// Log Builder class
    /// </summary>
    public class LogBuilder
    {
        /// <summary>
        /// Transforms an exception into a log message
        /// </summary>
        /// <param name="exception">An instance of <see cref="Exception"/></param>
        /// <returns>A the log message for the exception</returns>
        public static string GetLog(Exception exception)
        {
            var logMessage = new StringBuilder();
            var format = "{2}{0}: {1}";

            if (exception != null)
            {
                logMessage.AppendLine("[EXCEPTION]");
                logMessage.AppendFormat(format, "Exception: Message", exception.Message, string.Empty);
                logMessage.AppendFormat(format, "Source", exception.Source, ", ");
                logMessage.AppendFormat(format, "StackTrace", exception.StackTrace, ", ");

                var baseException = exception.GetBaseException();
                if (baseException != exception)
                {
                    logMessage.Append("***INNER EXCEPTION***");
                    logMessage.AppendFormat(format, "; Base Exception: Message", baseException.Message, ", ");
                    logMessage.AppendFormat(format, "Base Exception Source", baseException.Source, ", ");
                    logMessage.AppendFormat(format, "Base Exception StackTrace", baseException.StackTrace, ", ");
                }
            }

            return logMessage.ToString();
        }
    }
}
