// <copyright file="Scene.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Models
{
    using System;

    /// <summary>
    /// The Scene class
    /// </summary>
    public class Scene
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Scene"/> class
        /// </summary>
        public Scene()
        {
            this.SceneId = Guid.NewGuid();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Scene Id
        /// </summary>
        public Guid SceneId { get; set; }

        /// <summary>
        /// Gets or sets the Screenplay Id
        /// </summary>
        public Guid ScreenplayId { get; set; }

        /// <summary>
        /// Gets or sets the Scene Number
        /// </summary>
        public int SceneNumber { get; set; }

        /// <summary>
        /// Gets or sets the Scene Name
        /// </summary>
        public string SceneName { get; set; }

        /// <summary>
        /// Gets or sets the Through Line
        /// </summary>
        public string Throughline { get; set; }

        /// <summary>
        /// Gets or sets the Scene Description
        /// </summary>
        public string SceneDescription { get; set; }

        /// <summary>
        /// Gets or sets the Scene Beats
        /// </summary>
        public string SceneBeats { get; set; }

        #endregion
    }
}
