// <copyright file="Screenplay.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The Screenplay class
    /// </summary>
    public class Screenplay
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Screenplay"/> class
        /// </summary>
        public Screenplay()
        {
            this.ScreenplayId = Guid.NewGuid();
            this.Scenes = new List<Scene>();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Screenplay Id
        /// </summary>
        public Guid ScreenplayId { get; set; }

        /// <summary>
        /// Gets or sets the Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Author Name
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// Gets or sets the Screenplay Version
        /// </summary>
        public string ScreenplayVersion { get; set; }

        /// <summary>
        /// Gets or sets the Copyright
        /// </summary>
        public string Copyright { get; set; }

        /// <summary>
        /// Gets or sets the Log Line
        /// </summary>
        public string LogLine { get; set; }

        /// <summary>
        /// Gets or sets the Synopsis
        /// </summary>
        public string Synopsis { get; set; }

        /// <summary>
        /// Gets or sets a list of <see cref="Scene`"/> instances
        /// </summary>
        public List<Scene> Scenes { get; set; }

        #endregion
    }
}
