// <copyright file="IScreenplayService.cs" company="Stephen Joubert">
//    Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Interfaces.Service
{
    using System;
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// Screenplay Service interface
    /// </summary>
    public interface IScreenplayService
    {
        /// <summary>
        /// Gets all Screenplay records
        /// </summary>
        /// <returns>An list of <see cref='Screenplay'/> instances</returns>
        IList<Screenplay> Get();

        /// <summary>
        /// Gets a Screenplay record
        /// </summary>
        /// <param name="screenplayId">The Screenplay Id</param>
        /// <param name="includeScenes">Indicates whether to include scene records, false by default</param>
        /// <returns>An instance of the <see cref='Screenplay'/> class</returns>
        Screenplay GetSingle(Guid screenplayId, bool includeScenes = false);

        /// <summary>
        /// Adds a new Screenplay record
        /// </summary>
        /// <param name="screenplay">An instance of the <see cref="Screenplay"/> class</param>
        /// <returns>A saved instance of the <see cref="Screenplay"/> class</returns>
        Screenplay Save(Screenplay screenplay);

        /// <summary>
        /// Deletes a Screenplay record
        /// </summary>
        /// <param name="screenplayId">The Screenplay Id</param>
        /// <returns>True if successful</returns>
        bool Delete(Guid screenplayId);
    }
}
