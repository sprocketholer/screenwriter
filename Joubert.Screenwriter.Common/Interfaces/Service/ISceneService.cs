// <copyright file="ISceneService.cs" company="Stephen Joubert">
//    Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Interfaces.Service
{
    using System;
    using System.Collections.Generic;
    using Models;

    /// <summary>
    /// Scene Service interface
    /// </summary>
    public interface ISceneService
    {
        /// <summary>
        /// Gets all Scenes for a screenplay
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <returns>An list of <see cref='Scene'/> instances</returns>
        IList<Scene> Get(Guid screenplayId);

        /// <summary>
        /// Gets a Scene record
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="sceneId">The Scene Id</param>
        /// <returns>An instance of the <see cref='Scene'/> class</returns>
        Scene GetSingle(Guid screenplayId, Guid sceneId);

        /// <summary>
        /// Adds a new Scene record
        /// </summary>
        /// <param name="scene">An instance of the <see cref="Scene"/> class</param>
        /// <returns>A saved instance of the <see cref="Scene"/> class</returns>
        Scene Save(Scene scene);

        /// <summary>
        /// Deletes a Scene record
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="sceneId">The Scene Id</param>
        /// <returns>True if successful</returns>
        bool Delete(Guid screenplayId, Guid sceneId);
    }
}
