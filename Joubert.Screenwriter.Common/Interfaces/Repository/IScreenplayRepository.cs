// <copyright file="IScreenplayRepository.cs" company="Stephen Joubert">
//    Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Interfaces.Repository
{
    using System;
    using System.Collections.Generic;
    using Common.Models;

    /// <summary>
    /// Screenplay Repository interface
    /// </summary>
    public interface IScreenplayRepository
    {
        /// <summary>
        /// Gets all Screenplay records
        /// </summary>
        /// <returns>An list of <see cref='Screenplay'/> instances</returns>
        IList<Screenplay> Get();

        /// <summary>
        /// Gets a Screenplay record
        /// </summary>
        /// <param name="screenplayId">The Screenplay Id</param>
        /// <returns>An instance of the <see cref='Screenplay'/> class</returns>
        Screenplay GetSingle(Guid screenplayId);

        /// <summary>
        /// Adds a new Screenplay record
        /// </summary>
        /// <param name="screenplay">An instance of the <see cref="Screenplay"/> class</param>
        /// <returns>A saved instance of the <see cref="Screenplay"/> class</returns>
        Screenplay Insert(Screenplay screenplay);

        /// <summary>
        /// Updates a Screenplay record
        /// </summary>
        /// <param name="screenplay">An instance of the <see cref="Screenplay"/> class</param>
        /// <returns>A saved instance of the <see cref="Screenplay"/> class</returns>
        Screenplay Update(Screenplay screenplay);

        /// <summary>
        /// Deletes a Screenplay record
        /// </summary>
        /// <param name="screenplayId">The Screenplay Id</param>
        /// <returns>True if successful</returns>
        bool Delete(Guid screenplayId);
    }
}
