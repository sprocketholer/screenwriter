// <copyright file="ISceneRepository.cs" company="Stephen Joubert">
//    Copyright (c) Stephen Joubert All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common.Interfaces.Repository
{
    using System;
    using System.Collections.Generic;
    using Common.Models;

    /// <summary>
    /// Scene Repository interface
    /// </summary>
    public interface ISceneRepository
    {
        /// <summary>
        /// Gets all Scene records for a screenplay
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <returns>An list of <see cref='Scene'/> instances</returns>
        IList<Scene> Get(Guid screenplayId);

        /// <summary>
        /// Gets a Scene record
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="sceneId">The Scene Id</param>
        /// <returns>An instance of the <see cref='Scene'/> class</returns>
        Scene GetSingle(Guid screenplayId, Guid sceneId);

        /// <summary>
        /// Adds a new Scene record
        /// </summary>
        /// <param name="scene">An instance of the <see cref="Scene"/> class</param>
        /// <returns>A saved instance of the <see cref="Scene"/> class</returns>
        Scene Insert(Scene scene);

        /// <summary>
        /// Updates a Scene record
        /// </summary>
        /// <param name="scene">An instance of the <see cref="Scene"/> class</param>
        /// <returns>A saved instance of the <see cref="Scene"/> class</returns>
        Scene Update(Scene scene);

        /// <summary>
        /// Deletes a Scene record
        /// </summary>
        /// <param name="screenplayId">A screenplay Id</param>
        /// <param name="sceneId">The Scene Id</param>
        /// <returns>True if successful</returns>
        bool Delete(Guid screenplayId, Guid sceneId);
    }
}
