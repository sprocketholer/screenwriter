﻿// <copyright file="Settings.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.Common
{
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Static class to expose application settings and connection strings
    /// </summary>
    public static class Settings
    {
        #region Public Properties

        /// <summary>
        /// Gets the main database connection string
        /// </summary>
        public static string MainConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(Settings._mainConnectionString))
                {
                    Settings.LoadSettings();
                }

                return Settings._mainConnectionString;
            }
        }

        /// <summary>
        /// Gets the Logging Service Name
        /// </summary>
        public static string LoggingServiceName
        {
            get
            {
                if (string.IsNullOrEmpty(Settings._loggingServiceName))
                {
                    Settings.LoadSettings();
                }

                return Settings._loggingServiceName;
            }
        }
        
        #endregion

        #region Private Properties

        /// <summary>
        /// Gets or sets the main database connection string
        /// </summary>
        private static string _mainConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the Logging Service Name
        /// </summary>
        private static string _loggingServiceName { get; set; }

        #endregion

        #region Private Methods

        /// <summary>
        /// Load the settings from the configuration file
        /// </summary>
        private static void LoadSettings()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appSettings.json");
            var configuration = builder.Build();

            Settings._mainConnectionString = configuration["MainConnectionString"];
            Settings._loggingServiceName = configuration["LoggingServiceName"];
        }

        #endregion
    }
}