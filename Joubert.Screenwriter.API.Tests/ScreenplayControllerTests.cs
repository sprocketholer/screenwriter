﻿// <copyright file="ScreenplayControllerTests.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API.Tests
{
    using System;
    using Joubert.Screenwriter.API.Controllers;
    using Joubert.Screenwriter.Common.Interfaces.Service;
    using Joubert.Screenwriter.Common.Models;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Screenplay controller tests
    /// </summary>
    [TestFixture]
    public class ScreenplayControllerTests
    {
        #region Private Properties

        /// <summary>
        /// Gets or sets an instance of the <see cref="IScreenplayController"/> class
        /// </summary>
        private ScreenplayController _screenplayController { get; set; }

        #endregion

        #region Setup/Teardown

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var screenplay = new Screenplay() { ScreenplayId = Guid.Parse("63578df6-427e-44c2-9043-1e23efd56faa"), Title = "Lady Bird" };

            var screenplayService = Substitute.For<IScreenplayService>();
            screenplayService.Save(Arg.Any<Screenplay>()).Returns(screenplay);

            this._screenplayController = new ScreenplayController(screenplayService);
        }

        /// <summary>
        /// Teardown method
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._screenplayController = null;
        }

        #endregion

        #region Tests

        /// <summary>
        /// Tests PUT - Success
        /// </summary>
        [Test]
        public void ScreenplayController_Put_Success()
        {
            var screenplay = new Screenplay() { ScreenplayId = Guid.Parse("63578df6-427e-44c2-9043-1e23efd56faa"), Title = "Lady Bird" };

            var result = this._screenplayController.Put(Guid.Parse("63578df6-427e-44c2-9043-1e23efd56faa"), screenplay);

            Assert.That(result.Value.ScreenplayId, Is.EqualTo(Guid.Parse("63578df6-427e-44c2-9043-1e23efd56faa")));
        }

        /// <summary>
        /// Tests PUT - Fail
        /// </summary>
        [Test]
        public void ScreenplayController_Put_Fail()
        {
            var screenplay = new Screenplay() { ScreenplayId = Guid.Parse("63578df6-427e-44c2-9043-1e23efd56faa"), Title = "Lady Bird" };

            var result = this._screenplayController.Put(Guid.Parse("1df95004-d7c7-4201-b263-977e1a89629e"), screenplay);

            Assert.That(result.Value, Is.Null);
        }

        #endregion
    }
}
