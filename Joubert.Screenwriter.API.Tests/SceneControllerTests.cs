﻿// <copyright file="SceneControllerTests.cs" company="Stephen Joubert">
//     Copyright (c) Stephen Joubert. All rights reserved.
// </copyright>

namespace Joubert.Screenwriter.API.Tests
{
    using System;
    using Joubert.Screenwriter.API.Controllers;
    using Joubert.Screenwriter.Common.Interfaces.Service;
    using Joubert.Screenwriter.Common.Models;
    using NSubstitute;
    using NUnit.Framework;

    /// <summary>
    /// Scene controller unit test class
    /// </summary>
    [TestFixture]
    public class SceneControllerTests
    {
        #region Private Properties

        /// <summary>
        /// Gets or sets an instance of the <see cref="SceneController"/> class
        /// </summary>
        private SceneController _sceneController { get; set; }

        #endregion

        #region Setup/Teardown

        /// <summary>
        /// Setup method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var scene = new Scene() { SceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63"), ScreenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6") };

            var sceneService = Substitute.For<ISceneService>();
            sceneService.Save(Arg.Any<Scene>()).Returns(scene);

            this._sceneController = new SceneController(sceneService);
        }

        /// <summary>
        /// Teardown method
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._sceneController = null;
        }

        #endregion

        #region Tests

        /// <summary>
        /// Tests Post - Success
        /// </summary>
        [Test]
        public void SceneController_Post_Success()
        {
            var scene = new Scene() { SceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63"), ScreenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6") };
            var screenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6");

            var result = this._sceneController.Post(screenplayId, scene);

            Assert.That(result.Value.SceneId, Is.EqualTo(Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63")));
        }

        /// <summary>
        /// Tests Post - Success
        /// </summary>
        [Test]
        public void SceneController_Post_Bad_ScreenplayId_Fail()
        {
            var scene = new Scene() { SceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63"), ScreenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6") };
            var screenplayId = Guid.Parse("61aaa48b-4590-41ee-a74e-4cb8f421d3f5");

            var result = this._sceneController.Post(screenplayId, scene);

            Assert.That(result.Value, Is.Null);
        }

        /// <summary>
        /// Tests Put - Success
        /// </summary>
        [Test]
        public void SceneController_Put_Success()
        {
            var scene = new Scene() { SceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63"), ScreenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6") };
            var screenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6");
            var sceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63");

            var result = this._sceneController.Put(screenplayId, sceneId, scene);

            Assert.That(result.Value.SceneId, Is.EqualTo(Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63")));
        }

        /// <summary>
        /// Tests Put bad screenplay Id - Fail
        /// </summary>
        [Test]
        public void SceneController_Put_Bad_ScreenplayId_Fail()
        {
            var scene = new Scene() { SceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63"), ScreenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6") };
            var screenplayId = Guid.Parse("34e42b9a-b1c5-4208-89b7-8eaf39405468");
            var sceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63");

            var result = this._sceneController.Put(screenplayId, sceneId, scene);

            Assert.That(result.Value, Is.Null);
        }

        /// <summary>
        /// Tests Put bad scene Id - Fail
        /// </summary>
        [Test]
        public void SceneController_Put_Bad_SceneId_Fail()
        {
            var scene = new Scene() { SceneId = Guid.Parse("01031fe8-261d-4c96-bd11-385c68689c63"), ScreenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6") };
            var screenplayId = Guid.Parse("98a11ddd-fd16-4746-9165-901992fa97b6");
            var sceneId = Guid.Parse("34e42b9a-b1c5-4208-89b7-8eaf39405468");

            var result = this._sceneController.Put(screenplayId, sceneId, scene);

            Assert.That(result.Value, Is.Null);
        }

        #endregion
    }
}
